#!/bin/env python3
# coding: utf-8
# Marco André <marcoandre@gmail.com>
# Lista de exercícios 4b

# Exercícios sem for, apenas com while e sem funções embutidas.
# Você pode utilizar funções já desenvolvidas em outros exercícios

def media_anual(temperaturas):
    '''Receba uma lista com as temperaturas médias de cada mês
    e devolva uma lista com os números correspondentes aos meses que 
    possuem temperatura superior á média anual.'''
    soma = 0
    i = 0
    while i < len(temperaturas):
        soma += temperaturas[i]
        i += 1
    media = soma / len(temperaturas)

    meses_acima_media = []
    i = 0
    while i < len(temperaturas):
        if temperaturas[i] > media:
            meses_acima_media.append(i)
        i += 1
    return meses_acima_media

def maiores_13(idades,alturas):
    '''Esta função recebe as idades e alturas de diversas pessoas, em duas
    listas separadas e de igual comprimento.
    Calcule a media das alturas e retorne as alturas daqueles que possuem 
    'idades' maior que 13 e altura inferior a media da turma'''
    soma = 0
    i = 0
    while i < len(alturas):
        soma += alturas[i]
        i += 1
    media = soma / len(alturas)

    altura_maiores_13 = []
    i = 0
    while i < len(idades):
        if idades[i] > 13 and alturas[i] < media:
            altura_maiores_13.append(alturas[i])
        i += 1
    return altura_maiores_13

def media_saltos_lista(saltos):
    '''Receba uma lista com os saltos de um atleta e calcule a média dos 
    seus saltos, sabendo que o melhor e o pior saltos são desconsiderados.'''
    maior = saltos[0]
    menor = saltos[0]
    i = 1
    while i < len(saltos):
        if saltos[i] > maior:
            maior = saltos[i]
        if saltos[i] < menor:
            menor = saltos[i]
        i += 1
        
    i = 0
    soma = 0
    while i < len(saltos):
        soma += saltos[i]
        i += 1
        
    soma = soma - (maior + menor)
    media = soma / (len(saltos) - 2)
    return media

def é_primo(valor):
    ''' Verifique se o 'valor' informado é primo.
    Um número primo é aquele que é divisível apenas por ele mesmo e por 1'''
    if valor <= 1:
        return False
    else:
        i = 2
        while i < valor:
            if valor % i == 0:
                return False
            i += 1
        return True

def lista_de_primos(inicio,fim):
    '''Retorne uma lista de primos entre os valores informados, incluindo
    os limites'''
    if inicio < 2:
        inicio = 2

    lista_primos = []
    i = inicio
    while i < fim:
        if é_primo(i):
            lista_primos.append(i)
        i += 1
    return lista_primos

def Fibonacci(n):
    ''' Retorne uma lista com os n primeiros valores da série de Fibonacci.
    Fibonacci = 1,1,2,3,5,8,13,...'''
    a = 0 
    b = 1
    i = 1
    elementos = []
    while i <= n:
        a,b = b, a+b
        elementos.append(a)
        i += 1
    return elementos

def calcula_aumento_salario(salario_atual):
    ''' Calcule o aumento de salário de acordo com a seguinte tabela:
    - até 1 SM(inclusive): aumento de 20%
    - de 1 até 2 SM(inclusive): aumento de 15%
    - de 2 até 5 SM(inclusive): aumento de 10%
    - acima de 5 SM: aumento de 5% 
    Salário mínimo para referência: R$ 724,00
    '''
    salario_minimo = 724.00
    if salario_atual / salario_minimo <= 1:
        percentual_aumento = 20
    elif salario_atual / salario_minimo <= 2:
        percentual_aumento = 15
    elif salario_atual / salario_minimo <= 5:
        percentual_aumento = 10
    else:
        percentual_aumento = 5
    
    novo_salario = salario_atual + (salario_atual * percentual_aumento/100)    
    return round(novo_salario,2)

def altera_salarios(salarios):
    i = 0
    while i < len(salarios):
        salarios[i] = calcula_aumento_salario(salarios[i])
        i += 1
    return salarios

# Área de testes: só mexa aqui se souber o que está fazendo!
acertos = 0
total = 0 

def test(obtido, esperado):
    global acertos, total
    total += 1
    if obtido != esperado:
        prefixo = ' Falhou.'
    else:
        prefixo = ' Passou.'
        acertos += 1
    print ('%s Esperado: %s \tObtido: %s' % (prefixo,repr(esperado), 
        repr(obtido)))

def main():
    print(' Meses acima da média:')
    test(media_anual([20,20,20,20,20,20,20,20,20,20,20,20]), [])
    test(media_anual([25,20,20,20,20,20,20,20,20,20,20,20]), [0])
    test(media_anual([23,25,26,24,21,22,26,24,25,22,23,19]), [1,2,3,6,7,8])
    test(media_anual([19,20,21,20,17,18,19,20,22,21,20]), [1, 2, 3, 7, 8, 9, 10])
    test(media_anual([21,22,23,21,22,22,23,21,23,22,21,23,21]), [1,2,4,5,6,8,9,11])
    
    print(' Alturas abaixo da media:')
    test(maiores_13([13,13,14],[1.7,1.7,1.5]), [1.5])
    test(maiores_13([13,13,14,13],[1.7,1.7,1.5,1.6]), [1.5])
    test(maiores_13([14,20],[1.6,2]), [1.6])
    test(maiores_13([10,7,13,15,20,21],[1.7,1.21,1.65,2,1.9,1.5]), [1.5])
    test(maiores_13([14,15,16,17,18,30],[1.9,1.89,1.85,1.95,2,1.99]), [1.9,1.89,1.85])
    test(maiores_13([10,9,90,9,13,14,15],[1.25,1.3,1.7,1.41,1.5,1.55,1.7]), [])

    print(' Média dos saltos em lista:')
    test(media_saltos_lista([10,10,10,10,10]), 10)
    test(media_saltos_lista([9,9.1,8,7,6.9]), 8)
    test(media_saltos_lista([1,1,3,5,5]), 3)
    test(media_saltos_lista([10,10,9.9,10,10]), 10)

    print(' Lista de primos:')
    test(lista_de_primos(0,1), [])
    test(lista_de_primos(5,10), [5,7])
    test(lista_de_primos(10,20), [11,13,17,19])
    test(lista_de_primos(0,21), [2,3,5,7,11,13,17,19])
    test(lista_de_primos(43,102), [43,47,53,59,61,67,71,73,79,83,89,97,101])

    print(' Fibonacci:')
    test(Fibonacci(1), [1])
    test(Fibonacci(2), [1,1])
    test(Fibonacci(3), [1,1,2])
    test(Fibonacci(4), [1,1,2,3])

    print(' Aumenta salários:')
    test(altera_salarios([500,724.0,725.00,1448.00,1449.00,3620.00,3621.00,4000.00]), 
        [600.0, 868.8, 833.75, 1665.2, 1593.9, 3982.0, 3802.05, 4200.0])

if __name__ == '__main__':
    main()
    print("\n%d Testes, %d Ok, %d Falhas: Nota %.1f" %(total, acertos,
     total-acertos, float(acertos*10)/total))
    if total == acertos:
        print("Parabéns, seu programa rodou sem falhas!")
